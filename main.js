//Creamos una class con un objeto, que tenga como propiedad un array vacío.

class ToDoList {
  constructor(parent) {
    //Inicio estado
    this.state = {
      todos: []
    };
    //Creo referencias a los elementos necesarios:
    //Formulario:
    this.addForm = parent.querySelector("form.add");
    //Botón limpiar:
    this.cleanButton = parent.querySelector("button.clean");
    //Botón de borrar todo:
    this.destroyButton = parent.querySelector("button.destroy");
    //Lista de tareas:
    this.todoList = parent.querySelector("ul.todolist");

    if (
      !this.addForm ||
      !this.cleanButton ||
      !this.destroyButton ||
      !this.todoList
    ) {
      throw new Error("Faltan elementos. Revisa el HTML");
    }
    this.start();
  }

  start() {
    //Añadir un todo:
    //Cuando el formulario se envie, Submit,
    //ejecutamos esa función, a partir de un evento
    //prevenimos que se envíe a ningún lado el formulario, con preventDefault.
    this.addForm.addEventListener("submit", e => {
      e.preventDefault();
      const todoInput = this.addForm.elements.todotext;
      const todoText = todoInput.value;
      if (todoText.length > 0 && todoText.length < 200) {
        this.addToDo(todoText);
        todoInput.value = "";
      }
    });

    //Borrar todo:
    this.destroyButton.addEventListener("click", e => {
      if (
        prompt(
          "Escribe BORRAR para borrar toda la lista de TODOs. No habrá vuelta atrás."
        ) === "BORRAR"
      ) {
        this.deleteTodoList();
      }
    });
  }
  //Añadir un todo. "Text" define lo que define el todo: un texto.
  //Va a recibir un texto, y lo va a añadir a la lista.
  //Cada vez que reciba un texto nuevo, lo meterá al principio del array.

  addToDo(text) {
    const newToDo = {
      text: text,
      done: false
    };
    this.state.todos.unshift(newToDo);

    this.sync();
  }

  //Marcar un todo como hecho o pendiente, cambiando su estado.
  //Por el index sabemos cuál es su posición en la lista.

  toggleToDoStatus(index) {
    const todo = this.state.todos[index];
    todo.done = !todo.done;

    this.sync();
  }

  //Limpiar lista de todos
  //No necesita parámetro, porque ya sabe lo que tiene que hacer por si sola
  //Creo la constante cleanList para quedarnos con el nuevo array.

  cleanTodoList() {
    const cleanList = this.state.todos.filter(function(todo) {
      return !todo.done;
    });
    this.state.todos = cleanList;

    this.sync();
  }

  //Borrar todos los todos

  deleteTodoList() {
    this.state.todos = [];

    this.sync();
  }

  sync() {
    console.log(this.state.todos);
  }

  render() {}
}

const main = document.querySelector("main");
const todos = new ToDoList(main);